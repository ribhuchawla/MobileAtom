//
//  registerViewController.swift
//  Mobile Atoms
//
//  Created by Jitendra Kadam on 10/5/17.
//  Copyright © 2017 Jitendra Kadam. All rights reserved.
//

import UIKit

class registerViewController: UIViewController {
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var emailAdd: UITextField!
    @IBOutlet weak var phoneNo: UITextField!
    @IBOutlet weak var companyNo: UITextField!
    @IBOutlet weak var registerOutlet: UIButton!
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.0/255.0, green: 60.0/255.0, blue: 77.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.layer.frame.size.height = 20
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = nil
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        registerOutlet.backgroundColor = UIColor.white
        registerOutlet.layer.cornerRadius = 20
        registerOutlet.layer.shadowOffset = CGSize(width: 5, height: 5)
        registerOutlet.layer.shadowColor = UIColor.lightGray.cgColor
        registerOutlet.layer.shadowOpacity = 0.5
    }
    
    @IBAction func showTerms(_ sender: UIButton) {
        
        if UserDefaults.standard.value(forKey: "terms") as! String != ""{
        
        let alertMsg = UserDefaults.standard.value(forKey: "terms") as? NSString
        
        let alert = UIAlertController(title: "Terms & Conditions", message: alertMsg! as String, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
            
        }else{
            
            print("no terms")
        }

        
    }
    
    @IBAction func registerAction(_ sender: UIButton) {
        
        var request = URLRequest(url: URL(string: "http://eleads.mobileatoms.com:8080/gtw/anon/REGISTER/upsert/User")!)
        request.httpMethod = "POST"
        let postString = "apiKey=c5d6911c-6b31-11e5-84e6-02a5ca5ac903&searchConnectionFlag=true&gphVer=2&firstname=\(firstName)&middlename=pqr&lastname=\(lastName)&displayname=Jitendra&user_id=xyz3&email_id=\(emailAdd)&company=rst&work_phone=\(companyNo)&fb_access_token=123&fb_refresh_token=456"
        print(postString as Any)
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=", error?.localizedDescription as Any)
                
                let alert = UIAlertController(title: "Server down", message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            
            let responseString = String(data: data, encoding: .utf8)
            
            //print("status=", response?.value(forKey: "status") as Any)
            print("responseString = \(String(describing: responseString))")
            
        }
        task.resume()

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
