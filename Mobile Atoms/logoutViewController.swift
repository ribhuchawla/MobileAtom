//
//  logoutViewController.swift
//  Mobile Atoms
//
//  Created by Jitendra Kadam on 10/5/17.
//  Copyright © 2017 Jitendra Kadam. All rights reserved.
//

import UIKit

class logoutViewController: UIViewController {
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneNoLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var logoutOutlet: UIButton!
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.0/255.0, green: 60.0/255.0, blue: 77.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.layer.frame.size.height = 20
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = nil
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        logoutOutlet.backgroundColor = UIColor.white
        logoutOutlet.layer.cornerRadius = 20
        logoutOutlet.layer.shadowOffset = CGSize(width: 5, height: 5)
        logoutOutlet.layer.shadowColor = UIColor.lightGray.cgColor
        logoutOutlet.layer.shadowOpacity = 0.5

    }
    
    @IBAction func logoutAction(_ sender: UIButton) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
