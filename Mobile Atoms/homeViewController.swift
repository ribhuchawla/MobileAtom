//
//  homeViewController.swift
//  Mobile Atoms
//
//  Created by Jitendra Kadam on 10/5/17.
//  Copyright © 2017 Jitendra Kadam. All rights reserved.
//

import UIKit
import GoogleSignIn
import TwitterKit


class homeViewController: UIViewController, GIDSignInUIDelegate {

    @IBOutlet weak var mainLogo: UILabel!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var registerOutlet: UIButton!
    @IBOutlet weak var fbOutlet: UIButton!
    @IBOutlet weak var linkedInOutlet: UIButton!
    @IBOutlet weak var twitOutlet: UIButton!
    @IBOutlet weak var googleOutlet: UIButton!
    @IBOutlet weak var conferenceName: UILabel!
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
        
        DispatchQueue.main.async {
            // Update UI
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        GIDSignIn.sharedInstance().uiDelegate = self
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(homeViewController.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        
        //statusText.text = "Initialized Swift app..."
        toggleAuthUI()
        
        // Uncomment to automatically sign in the user.
        //GIDSignIn.sharedInstance().signInSilently()
        
        // TODO(developer) Configure the sign-in button look/feel
        // ...
        
        self.navigationController?.navigationBar.isHidden = true
        
        fbOutlet.layer.cornerRadius = 20
        linkedInOutlet.layer.cornerRadius = 20
        twitOutlet.layer.cornerRadius = 20
        googleOutlet.layer.cornerRadius = 20
        
        titleView.layer.shadowOffset = CGSize(width: 5, height: 5)
        titleView.layer.shadowColor = UIColor.lightGray.cgColor
        titleView.layer.shadowOpacity = 0.5
        
        mainLogo.layer.borderColor = UIColor.white.cgColor
        mainLogo.layer.borderWidth = 1
        mainLogo.layer.cornerRadius = 5
        
        registerOutlet.backgroundColor = UIColor.white
        registerOutlet.layer.cornerRadius = 20
        registerOutlet.layer.shadowOffset = CGSize(width: 5, height: 5)
        registerOutlet.layer.shadowColor = UIColor.lightGray.cgColor
        registerOutlet.layer.shadowOpacity = 0.5
        
        /*fbOutlet.contentMode = .center
        fbOutlet.imageView?.contentMode = .right
        fbOutlet.semanticContentAttribute = .forceRightToLeft
        fbOutlet.setImage(#imageLiteral(resourceName: "facebook.png"), for: .normal)*/
        
        var request = URLRequest(url: URL(string: "http://eleads.mobileatoms.com:8080/gtw/anon/REGISTER/terms/Default")!)
        request.httpMethod = "POST"
        let postString = "apiKey=c5d6911c-6b31-11e5-84e6-02a5ca5ac903&searchConnectionFlag=true&gphVer=2"
        print(postString as Any)
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=", error?.localizedDescription as Any)
                
                let alert = UIAlertController(title: "Server down", message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                return
            }
            
            let response : AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
            
            let urlData = try! NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            
            let json = try! JSONSerialization.jsonObject(with: urlData, options: JSONSerialization.ReadingOptions.mutableContainers)
            
            let dict = json as! NSDictionary
            
            //print(dict as NSDictionary)
            
            //print(dict.value(forKey: "resultSet") as Any)
            
            if dict.value(forKey: "resultSet") != nil{
                
                let resultSet : NSDictionary = dict.value(forKey: "resultSet") as! NSDictionary
                
                if resultSet.value(forKey: "results") != nil{
                    
                    let results : NSDictionary = resultSet.value(forKey: "results") as! NSDictionary
                    
                    if results.value(forKey: "CONFERENCE_NAME") != nil {
                        
                        self.conferenceName.text = results.value(forKey: "CONFERENCE_NAME") as? String
                        
                        print(self.conferenceName.text!, "is title")
                        
                        if results.value(forKey: "TERMS_TEXT") != nil{
                            
                            UserDefaults.standard.set(results.value(forKey: "TERMS_TEXT"), forKey: "terms")
                            
                            //print(UserDefaults.standard.value(forKey: "terms") as Any)
                            
                        }else{
                            
                            print("no terms")
                        }
                        
                        
                    }else{
                        
                        print("no conference name")
                    }
                    
                    
                }else{
                    
                    print("results are nil")
                }
                
            }else{
                
                print("resultset is nil")
            }
            
        }
        
        task.resume()
    }
    
    @IBAction func didTapSignOut(_ sender: AnyObject) {
        GIDSignIn.sharedInstance().signOut()
        // [START_EXCLUDE silent]
        //statusText.text = "Signed out."
        toggleAuthUI()
        // [END_EXCLUDE]
    }
    // [END signout_tapped]
    // [START disconnect_tapped]
    @IBAction func didTapDisconnect(_ sender: AnyObject) {
        GIDSignIn.sharedInstance().disconnect()
        // [START_EXCLUDE silent]
        //statusText.text = "Disconnecting."
        // [END_EXCLUDE]
    }
    // [END disconnect_tapped]
    // [START toggle_auth]
    func toggleAuthUI() {
        if GIDSignIn.sharedInstance().hasAuthInKeychain() {
            // Signed in
            googleOutlet.isHidden = true
            //signOutButton.isHidden = false
            //disconnectButton.isHidden = false
        } else {
            googleOutlet.isHidden = false
            /*signOutButton.isHidden = true
            disconnectButton.isHidden = true
            statusText.text = "Google Sign in\niOS Demo"*/
        }
    }
    // [END toggle_auth]
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                                  object: nil)
    }
    
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                print(userInfo)
                //self.statusText.text = userInfo["statusText"]!
            }
        }
    }
    

    @IBAction func registerAction(_ sender: UIButton) {
        
       
        
    }
    @IBAction func registerWithfacebook(_ sender: UIButton) {
        
        
        
    }
    @IBAction func registerWithLinkedIn(_ sender: UIButton) {
        
    }
    @IBAction func registerWithTwitter(_ sender: UIButton) {
        Twitter.sharedInstance().logIn(completion: { (session, error) in
            print("Twitter login pressed");
            if (session != nil) {
                print("signed in as \(session?.userName)");
            } else {
                print("error: \(error?.localizedDescription)");
            }
        })
    }
    
    @IBAction func registerGoogle(_ sender: Any) {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
